#!/bin/sh -e

provider="${1}"
if [ -z "${provider}" ]; then
  echo 'no provider defined, using by default exo.cat'
  provider="exo.cat"
fi

lang="${2}"
if [ -z "${lang}" ]; then
  echo 'no lang defined, using by default en'
  lang="en"
fi

for prov in $(ls "providers/${provider}"); do
  prop="$(echo ${prov} | cut -d'_' -f1)"
  boolean="$(echo ${prov} | cut -d'_' -f2 | cut -d'.' -f1)"
  cat "propositions/${prop}/${lang}.md"
  # TODO warning if empty file, means evidence is missing
  echo "  ${boolean}"

done
