## masmovil, vodafone: true

```
$ echo | openssl s_client -connect gateway.ipfs.io:443 | grep issuer
depth=1 C = ES, ST = Madrid, L = Madrid, O = Allot, OU = Allot, CN = allot.com/emailAddress=info@allot.com
verify error:num=19:self signed certificate in certificate chain
verify return:1
depth=1 C = ES, ST = Madrid, L = Madrid, O = Allot, OU = Allot, CN = allot.com/emailAddress=info@allot.com
verify return:1
depth=0 CN = gateway.ipfs.io
verify return:1
issuer=C = ES, ST = Madrid, L = Madrid, O = Allot, OU = Allot, CN = allot.com/emailAddress=info@allot.com
DONE
```

## orange: false

```
depth=2 C = US, O = Internet Security Research Group, CN = ISRG Root X1
verify return:1
depth=1 C = US, O = Let's Encrypt, CN = R3
verify return:1
depth=0 CN = dweb.link
verify return:1
DONE
issuer=C = US, O = Let's Encrypt, CN = R3
```
