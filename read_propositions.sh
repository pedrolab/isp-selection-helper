#!/bin/sh -e

lang="${1}"
if [ -z "${lang}" ]; then
  lang="en"
fi

grep --color=auto -H . propositions/*/${lang}.md
