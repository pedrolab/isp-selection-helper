this is a social experiment

[TOC]

selection-helper wants to be a generic tool and structure to facilitate choosing around different solutions

## content format

All content is formatted in [markdown format](https://en.wikipedia.org/wiki/Markdown)

## propositions

[a proposition](https://en.wikipedia.org/wiki/Proposition) is a sentence that is answered with true or false

### structure

every proposition is a directory which is name is a unique natural number

example: here you have 3 propositions that are translated to Catalan (ca), English (en) and Spanish (es)

```
propositions/
├── 1
│   ├── ca
│   ├── en
│   └── es
├── 2
│   ├── ca
│   ├── en
│   └── es
├── 3
│   ├── ca
│   ├── en
│   └── es
```

### including a new translation

find your language subtag on the following URL http://www.iana.org/assignments/language-subtag-registry/language-subtag-registry

more info, check out https://en.wikipedia.org/wiki/IETF_language_tag

## providers

provider's identifier is its [TLD](https://en.wikipedia.org/wiki/Top-level_domain)

a provider responds true or false to a certain proposition in a file that contain one line per evidence

### how provider answers a proposition

Writes on the provider's directory a file which is name is its reference number followed by a `_` and true or false. Example: answers proposition 1 with false would be `providersTLD/1_false`

If the file has no content, no evidence is provided

If different evidences are attached, separate them with `---`

English is preferred for answering the propositions, reason is that the information for the final user only wants simple information: if *something* comply with a proposition

Example for evidence content:

```
https://example.com/path-to-evidence
---
https://example.org/path-to-another-evidence
---

And this is text content that is an evidence itself
blabla
blabla
blabla
---

And this is another evidence
blabla
blabla
```

### how to discuss around a proposition for a provider

If a proposition is both true and false for a provider, we inform the final user of the discussion. Discussion file `n_discussion` contains references one line for each one, containing where it is currently being discussed

```
providers/
├── exo.cat
│   └── 1_false
│   └── 1_true
│   └── 1_discussion
```

### LICENSE

License by default is CC BY SA 4.0 unless explicit license in certain file
